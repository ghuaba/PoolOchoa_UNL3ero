/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import controlador.Exception.VacioException;
import controlador.listaEnlazada.ListaEnlazada;

/**
 *
 * @author ghuaba
 */
public class OrdenacionDao {

    public OrdenacionDao() {
        this.lista.llenarAleatoriamente(20000, 1, 500);
    }

//
//    public ListaEnlazada<Integer> ordenarQuickAs(ListaEnlazada<Integer> lista) {
//        try {
//            Integer[] matriz = lista.toArray();
//
//            quickSort(matriz, 0, matriz.length - 1);
//
//            lista.toList(matriz);
//        } catch (Exception e) {
//        }
//        return lista;
//    }
//
//    
//    
//    
//    public ListaEnlazada<Integer> ordenarQuickDes(ListaEnlazada<Integer> lista) {
//        try {
//            Integer[] matriz = lista.toArray();
//
//            quickSortDes(matriz, 0, matriz.length - 1);
//
//            lista.toList(matriz);
//        } catch (Exception e) {
//        }
//        return lista;
//    }
//
//    
//    public ListaEnlazada<Integer> ordenarQuick(ListaEnlazada<Integer> lista, int tipoOrdenacion) {
//    try {
//        Integer[] matriz = lista.toArray();
//        
//        switch (tipoOrdenacion) {
//            case 0:
//                quickSort(matriz, 0, matriz.length - 1);
//                break;
//            case 1:
//                quickSortDes(matriz, 0, matriz.length - 1);
//                break;
//         
//        }
//        
//        lista.toList(matriz);
//    } catch (Exception e) {
//    }
//    return lista;
//}
    public void quickSort(Integer[] numerosAOrdenar, int izq, int der) {
        int pivote = numerosAOrdenar[izq];
        int i = izq;
        int j = der;
        int auxiliar;

        while (i < j) {
            while (numerosAOrdenar[i] <= pivote && i < j) {
                i++;  ////// cambiar a >=
            }
            while (numerosAOrdenar[j] > pivote) {
                j--;          //y este talvez <
            }
            if (i < j) {
                auxiliar = numerosAOrdenar[i];
                numerosAOrdenar[i] = numerosAOrdenar[j];
                numerosAOrdenar[j] = auxiliar;
            }
        }

        numerosAOrdenar[izq] = numerosAOrdenar[j];
        numerosAOrdenar[j] = pivote;

        if (izq < j - 1) {
            quickSort(numerosAOrdenar, izq, j - 1);
        }
        if (j + 1 < der) {
            quickSort(numerosAOrdenar, j + 1, der);
        }

    }

    public void quickSortDes(Integer numerosAOrdenar[], int izq, int der) {
        int pivote = numerosAOrdenar[izq];
        int i = izq;
        int j = der;
        int auxiliar;

        while (i < j) {
            while (numerosAOrdenar[i] >= pivote && i < j) {  // Cambio a >=
                i++;
            }
            while (numerosAOrdenar[j] < pivote) {  // Cambio a <
                j--;
            }
            if (i < j) {
                auxiliar = numerosAOrdenar[i];
                numerosAOrdenar[i] = numerosAOrdenar[j];
                numerosAOrdenar[j] = auxiliar;
            }
        }

        numerosAOrdenar[izq] = numerosAOrdenar[j];
        numerosAOrdenar[j] = pivote;

        if (izq < j - 1) {
            quickSortDes(numerosAOrdenar, izq, j - 1);
        }
        if (j + 1 < der) {
            quickSortDes(numerosAOrdenar, j + 1, der);
        }
    }

    private void merge(Integer arr[], int l, int m, int r) {

        int n1 = m - l + 1;
        int n2 = r - m;

        int L[] = new int[n1];
        int R[] = new int[n2];

        // Copy data to temp arrays
        for (int i = 0; i < n1; ++i) {
            L[i] = arr[l + i];
        }
        for (int j = 0; j < n2; ++j) {
            R[j] = arr[m + 1 + j];
        }

        int i = 0, j = 0;

        int k = l;
        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }

        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    // Main function that sorts arr[l..r] using
    // merge()
    public void mergeSort(Integer arr[], int l, int r) {
        if (l < r) {

            // Find the middle point
            int m = l + (r - l) / 2;

            mergeSort(arr, l, m);
            mergeSort(arr, m + 1, r);

            merge(arr, l, m, r);
        }
    }

     public void mergeSortDesc(Integer arr[], int l, int r) {
        if (l < r) {

            int m = l + (r - l) / 2;

            mergeSortDesc(arr, l, m);
            mergeSortDesc(arr, m + 1, r);

            mergeDesc(arr, l, m, r);
        }
    }
    private void mergeDesc(Integer arr[], int l, int m, int r) {

        int n1 = m - l + 1;
        int n2 = r - m;

        // Create temp arrays
        int L[] = new int[n1];
        int R[] = new int[n2];

        // Copy data to temp arrays
        for (int i = 0; i < n1; ++i) {
            L[i] = arr[l + i];
        }
        for (int j = 0; j < n2; ++j) {
            R[j] = arr[m + 1 + j];
        }

        int i = 0, j = 0;

        int k = l;
        while (i < n1 && j < n2) {
            if (L[i] >= R[j]) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }

        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }

    private ListaEnlazada<Integer> lista = new ListaEnlazada();

    public ListaEnlazada<Integer> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Integer> lista) {
        this.lista = lista;
    }

}
