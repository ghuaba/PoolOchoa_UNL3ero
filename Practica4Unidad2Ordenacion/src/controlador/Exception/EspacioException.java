/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.Exception;

/**
 *
 * @author ghuaba
 */
public class EspacioException extends Exception{
    
    public EspacioException(String msg) {
        super(msg);
    }
    public EspacioException() {
        super("EL arreglo esta lleno o posicion no valida");
    }
}