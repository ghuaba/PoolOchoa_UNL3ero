/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista;

import controlador.listaEnlazada.ListaEnlazada;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ghuaba
 */

public class ModeloTabla extends AbstractTableModel {


    
private ListaEnlazada<Integer> lista = new ListaEnlazada<>();

    public ListaEnlazada<Integer> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Integer> lista) {
        this.lista = lista;
    }

    @Override
    public int getRowCount() {
        return lista.size();
        }

    @Override
    public int getColumnCount() {
        return 1;
    }

  @Override
    public Object getValueAt(int row, int column) {
        Integer values = null;
        try {
             values = lista.get(row);
        } catch (Exception e) {

        }
        switch (column) {
            case 0: return values;

            default: return null;
              
        }
    }
    @Override
    public String getColumnName(int column) {
        switch(column){
            case 0: return "Valores";
            default: return null;
        }
    }   
}

