/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.busqueda;

import controlador.dao.CelularDAO;
import controlador.lista.ListaEnlazada;
import modelo.Celular;

/**
 *
 * @author ghuaba
 */
public class BinaryLineal {

    public ListaEnlazada<Celular> binaryLinealCelular(String atributo) throws Exception {
        CelularDAO celDao = new CelularDAO();
        ListaEnlazada<Celular> listaCel = new ListaEnlazada<>();
        ListaEnlazada<Celular> lista = celDao.listar();
        celDao.ordenarNombreModelo(lista);
        int inferior = 0;
        int superior = lista.size() - 1;
        Celular auxCel;
        while (inferior <= superior) {
            int centro = (inferior + superior) / 2;
            auxCel = lista.get(centro);

            int comparacion = auxCel.getModelo().compareToIgnoreCase(atributo);

            if (comparacion == 0) {
                listaCel.insertar(auxCel);
                // Continuar la búsqueda en la mitad inferior de la lista
                int i = centro - 1;
                while (i >= inferior) {
                    auxCel = lista.get(i);
                    if (auxCel.getModelo().equalsIgnoreCase(atributo)) {
                        listaCel.insertar(auxCel);
                    } else {
                        break;
                    }
                    i--;
                }
                // Continuar la búsqueda en la mitad superior de la lista
                int j = centro + 1;
                while (j <= superior) {
                    auxCel = lista.get(j);
                    if (auxCel.getModelo().equalsIgnoreCase(atributo)) {
                        listaCel.insertar(auxCel);
                    } else {
                        break;
                    }
                    j++;
                }
                break;
            } else if (comparacion < 0) {
                inferior = centro + 1;
            } else {
                superior = centro - 1;
            }
        }
        return listaCel;
    }

    public ListaEnlazada<Celular> binaryLinealId(String atributo) throws Exception {
        CelularDAO celDao = new CelularDAO();
        ListaEnlazada<Celular> listaCel = new ListaEnlazada<>();
        ListaEnlazada<Celular> lista = celDao.listar();

        celDao.ordenarNombreID(lista);

        int inferior = 0;
        int superior = lista.size() - 1;
        Celular auxCel;

        while (inferior <= superior) {
            int centro = (inferior + superior) / 2;
            auxCel = lista.get(centro);

            int comparacion = auxCel.getId_dispositivo().compareToIgnoreCase(atributo);

            if (comparacion == 0) {
                listaCel.insertar(auxCel);
                // Continuar la búsqueda en la mitad inferior de la lista
                int i = centro - 1;
                while (i >= inferior) {
                    auxCel = lista.get(i);
                    if (auxCel.getId_dispositivo().equalsIgnoreCase(atributo)) {
                        listaCel.insertar(auxCel);
                    } else {
                        break;
                    }
                    i--;
                }
                // Continuar la búsqueda en la mitad superior de la lista
                int j = centro + 1;
                while (j <= superior) {
                    auxCel = lista.get(j);
                    if (auxCel.getId_dispositivo().equalsIgnoreCase(atributo)) {
                        listaCel.insertar(auxCel);
                    } else {
                        break;
                    }
                    j++;
                }
                break;
            } else if (comparacion < 0) {
                inferior = centro + 1;
            } else {
                superior = centro - 1;
            }
        }
        return listaCel;
    }

}
