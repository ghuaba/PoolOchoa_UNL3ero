/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.busqueda;

import controlador.dao.CelularDAO;
import controlador.dao.MarcaDAO;
import controlador.dao.PaisFabricacionDAO;
import controlador.lista.ListaEnlazada;
import controlador.lista.NodoLista;
import modelo.Celular;
import modelo.Marca;
import modelo.PaisFabricacion;

/**
 *
 * @author ghuaba
 */
public class Binaria {

    
    //lo suelo usar de guia
    private ListaEnlazada<Celular> binarySearchOficial(String atributo) throws Exception {
        CelularDAO celDao = new CelularDAO();
        ListaEnlazada<Celular> listaCel = new ListaEnlazada<>();
        ListaEnlazada<Celular> lista = celDao.listar();
        celDao.ordenarNombreModelo(lista);

        int inferior = 0;
        int superior = lista.size() - 1;
        while (inferior <= superior) {
            int centro = (inferior + superior) / 2;
            Celular auxCel = lista.get(centro);
            int comparacion = auxCel.getModelo().compareToIgnoreCase(atributo);
            if (comparacion == 0) {
                listaCel.insertar(auxCel);
                break;
            } else if (comparacion < 0) {
                inferior = centro + 1;
            } else {
                superior = centro - 1;
            }
        }
        return listaCel;
    }
 
    public ListaEnlazada<Celular> binarySearchCelular(String atributo) throws Exception {
        CelularDAO celDao = new CelularDAO();
        ListaEnlazada<Celular> listaCel = new ListaEnlazada<>();
        ListaEnlazada<Celular> lista = celDao.listar();
        Celular auxCel;
        celDao.ordenarNombreModelo(lista);

        int inferior = 0;
        int superior = lista.size() - 1;

        while (inferior <= superior) {
            int centro = (inferior + superior) / 2;
            auxCel = lista.get(centro);
            int comparacion = auxCel.getModelo().compareToIgnoreCase(atributo);
            if (comparacion == 0) {
                listaCel.insertar(auxCel);
                break;
            } else if (comparacion < 0) {
                inferior = centro + 1;
            } else {
                superior = centro - 1;
            }
        }
        return listaCel;
    }

    

    
    
       public ListaEnlazada<Celular> binarySearchMarca(String nombreMarca) throws Exception {
        ListaEnlazada<Celular> listaC = new ListaEnlazada<>();

        MarcaDAO marcaDao = new MarcaDAO();
        Marca marcaBuscada = marcaDao.buscarMarcaBinaria(nombreMarca.trim());

        if (marcaBuscada != null) {
            CelularDAO celularDao = new CelularDAO();
            ListaEnlazada<Celular> listaCelulares = celularDao.listar();

            NodoLista<Celular> nodoActual = listaCelulares.getCabecera();
            while (nodoActual != null) {
                Celular celular = nodoActual.getInfo();
                if (celular.getId_marca().intValue() == marcaBuscada.getId().intValue()) {
                    listaC.insertar(celular);
                    break;
                }
                nodoActual = nodoActual.getSig();
            }
        }

        return listaC;
    }
    
       
       public ListaEnlazada<Celular> binarySearchPais(String nombrePais) throws Exception {
        ListaEnlazada<Celular> listaP = new ListaEnlazada<>();

        PaisFabricacionDAO paisDao = new PaisFabricacionDAO();
        PaisFabricacion paisBuscado = paisDao.buscarPaisBinaria(nombrePais.trim());

        if (paisBuscado != null) {
            CelularDAO celularDao = new CelularDAO();
            ListaEnlazada<Celular> listaCelulares = celularDao.listar();

            NodoLista<Celular> nodoActual = listaCelulares.getCabecera();
            while (nodoActual != null) {
                Celular celular = nodoActual.getInfo();
                if (celular.getId_paisFabriacion().intValue() == paisBuscado.getId().intValue()) {
                    listaP.insertar(celular);
                    break;
                }
                nodoActual = nodoActual.getSig();
            }
        }

        return listaP;
    }
    
       
        public ListaEnlazada<Celular> binarySearchId(String atributo) throws Exception {
        CelularDAO celDao = new CelularDAO();
        ListaEnlazada<Celular> listaCel = new ListaEnlazada<>();
        ListaEnlazada<Celular> lista = celDao.listar();
        Celular auxCel;
        celDao.ordenarNombreID(lista);

        int inferior = 0;
        int superior = lista.size() - 1;

        while (inferior <= superior) {
            int centro = (inferior + superior) / 2;
            auxCel = lista.get(centro);

            int comparacion = auxCel.getId_dispositivo().compareToIgnoreCase(atributo);

            if (comparacion == 0) {
                listaCel.insertar(auxCel);
                break;
            } else if (comparacion < 0) {
                inferior = centro + 1;
            } else {
                superior = centro - 1;
            }
        }
        return listaCel;
    }
       
}
