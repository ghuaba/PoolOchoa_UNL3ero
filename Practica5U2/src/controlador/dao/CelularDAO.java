/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.dao;

import controlador.exception.PosicionException;
import controlador.exception.VacioException;
import controlador.lista.ListaEnlazada;
import java.io.IOException;
import modelo.Celular;
import modelo.Marca;
import modelo.PaisFabricacion;

/**
 *
 * @author ghuaba
 */
public class CelularDAO extends AdaptadorDAO<Celular> {

    private Celular celular;

    public CelularDAO() {
        super(Celular.class);
    }

    public Celular getCelular() {
        if (this.celular == null) {
            this.celular = new Celular();
        }
        return celular;
    }

    public void setCelular(Celular celular) {
        this.celular = celular;
    }

    public void guardar() throws IOException {
        celular.setId(generateID());
        this.guardar(celular);
    }

    public void modificar(Integer pos) throws VacioException, PosicionException, IOException {
        this.modificar(celular, pos);
    }

    private Integer generateID() {
        return listar().size() + 1;
    }

    
    
    public ListaEnlazada<Celular> ordenarNombreModelo(ListaEnlazada<Celular> lista) {
        try {
            Celular[] matriz = lista.toArray();
            for (int i = 1; i < lista.size(); i++) {
                Celular key = lista.get(i);
                int j = i - 1;
                while (j >= 0 && (matriz[j].getModelo().compareToIgnoreCase(key.getModelo())) > 0) {
                    //lista.update(j+1, lista.get(j));
                    matriz[j + 1] = matriz[j];
                    j = j - 1;
                }
                //lista.update(j+1, key);
                matriz[j + 1] = key;
            }
            lista.toList(matriz);
        } catch (Exception e) {
        }
        return lista;
    }

    //cambia de array a lista
    public ListaEnlazada<Celular> ordenarNombreModeloArray(ListaEnlazada<Celular> lista) {
        try {
            Celular[] matriz = lista.toArray();
            for (int i = 1; i < lista.size(); i++) {
                Celular key = matriz[i];
                int j = i - 1;
                while (j >= 0 && (matriz[j].getModelo().compareToIgnoreCase(key.getModelo())) > 0) {
                    //lista.update(j+1, lista.get(j));
                    matriz[j + 1] = matriz[j];
                    j = j - 1;
                }
                //lista.update(j+1, key);
                matriz[j + 1] = key;
            }
            lista.toList(matriz);
        } catch (Exception e) {
        }
        return lista;
    }


    
      public ListaEnlazada<Celular> ordenarNombreID(ListaEnlazada<Celular> lista) {
        try {
            Celular[] matriz = lista.toArray();
            for (int i = 1; i < lista.size(); i++) {
                Celular key = lista.get(i);
                int j = i - 1;
                while (j >= 0 && (matriz[j].getId_dispositivo().compareToIgnoreCase(key.getId_dispositivo())) > 0) {
                    //lista.update(j+1, lista.get(j));
                    matriz[j + 1] = matriz[j];
                    j = j - 1;
                }
                //lista.update(j+1, key);
                matriz[j + 1] = key;
            }
            lista.toList(matriz);
        } catch (Exception e) {
        }
        return lista;
    }
    

    public ListaEnlazada<Celular> searchLinealMarca(String dato) throws Exception {
        ListaEnlazada<Celular> resultados = new ListaEnlazada<>();
        Marca g = new MarcaDAO().buscarMarcaBinaria(dato.trim());
        if (g != null) {
            ListaEnlazada<Celular> lista = listar();
            for (int i = 0; i < lista.size(); i++) {
                Celular aux = lista.get(i);
                if (aux.getId_marca().intValue() == g.getId().intValue()) {
                    resultados.insertar(aux);
                }

            }
        }
        return resultados;
    }
    
    
    
        public ListaEnlazada<Celular> searchLinealPais(String dato) throws Exception {
        ListaEnlazada<Celular> resultados = new ListaEnlazada<>();
        PaisFabricacion g = new PaisFabricacionDAO().buscarPaisBinaria(dato.trim());
        if (g != null) {
            ListaEnlazada<Celular> lista = listar();
            for (int i = 0; i < lista.size(); i++) {
                Celular aux = lista.get(i);
                if (aux.getId_paisFabriacion().intValue() == g.getId().intValue()) {
                    resultados.insertar(aux);
                }

            }
        }
        return resultados;
    }

}
