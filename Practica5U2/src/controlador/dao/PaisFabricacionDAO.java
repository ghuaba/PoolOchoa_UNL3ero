/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.dao;

import controlador.exception.PosicionException;
import controlador.exception.VacioException;
import controlador.lista.ListaEnlazada;
import java.io.IOException;
import modelo.Marca;
import modelo.PaisFabricacion;

/**
 *
 * @author ghuaba
 */
public class PaisFabricacionDAO extends AdaptadorDAO<PaisFabricacion> {

    private PaisFabricacion paisFabricacion;

    public PaisFabricacionDAO() {
        super(PaisFabricacion.class);
    }

    public PaisFabricacion getPaisFabricacion() {
        if (this.paisFabricacion == null) {
            this.paisFabricacion = new PaisFabricacion();
        }
        return paisFabricacion;
    }

    public void setPaisFabricacion(PaisFabricacion paisFabricacion) {
        this.paisFabricacion = paisFabricacion;
    }

    public void guardar() throws IOException {
        paisFabricacion.setId(generateID());
        this.guardar(paisFabricacion);
    }

    public void modificar(Integer pos) throws VacioException, PosicionException, IOException {
        this.modificar(paisFabricacion, pos);
    }

    private Integer generateID() {
        return listar().size() + 1;
    }

    //en lista enlazada
    public ListaEnlazada<PaisFabricacion> ordenarNombrePais(ListaEnlazada<PaisFabricacion> lista) {
        try {
            PaisFabricacion[] matriz = lista.toArray();
            for (int i = 1; i < lista.size(); i++) {
                PaisFabricacion key = lista.get(i);
                int j = i - 1;
                while (j >= 0 && (matriz[j].getNombre().compareToIgnoreCase(key.getNombre())) > 0) {
                    //lista.update(j+1, lista.get(j));
                    matriz[j + 1] = matriz[j];
                    j = j - 1;
                }
                //lista.update(j+1, key);
                matriz[j + 1] = key;
            }
            lista.toList(matriz);
        } catch (Exception e) {
        }
        return lista;
    }

    
    
    //en arreglo
    public ListaEnlazada<PaisFabricacion> ordenarNombrePaisArreglo(ListaEnlazada<PaisFabricacion> lista) {
        try {
            PaisFabricacion[] matriz = lista.toArray();
            for (int i = 1; i < lista.size(); i++) {
                PaisFabricacion key = matriz[i]; ///aqi cambiaaaaaaaaa
                int j = i - 1;
                while (j >= 0 && (matriz[j].getNombre().compareToIgnoreCase(key.getNombre())) > 0) {
                    //lista.update(j+1, lista.get(j));
                    matriz[j + 1] = matriz[j];
                    j = j - 1;
                }
                //lista.update(j+1, key);
                matriz[j + 1] = key;
            }
            lista.toList(matriz);
        } catch (Exception e) {
        }
        return lista;
    }

    
    
    public PaisFabricacion buscarNombre(String dato) throws Exception {
        PaisFabricacion resultado = null;
        ListaEnlazada<PaisFabricacion> lista = listar();
        for (int i = 0; i < lista.size(); i++) {
            PaisFabricacion aux = lista.get(i);

            if (aux.getNombre().toLowerCase().equals(dato.toLowerCase())) {
                resultado = aux;
                break;
            }
        }
        return resultado;
    }

      public PaisFabricacion buscarPaisBinaria(String atributo) throws Exception {
    ListaEnlazada<PaisFabricacion> listaPais = listar();
    listaPais = ordenarNombrePais(listaPais);
    
    int left = 0;
    int right = listaPais.size() - 1;
    
    while (left <= right) {
        int mid = left + (right - left) / 2;
        PaisFabricacion paisActual = listaPais.get(mid);
        
        int comparacion = paisActual.getNombre().compareToIgnoreCase(atributo);
        
        if (comparacion == 0) {
            return paisActual;
        } else if (comparacion < 0) {
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }
    
    return null;
}
}
