/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.dao;

import controlador.exception.PosicionException;
import controlador.exception.VacioException;
import controlador.lista.ListaEnlazada;
import controlador.lista.NodoLista;
import java.io.IOException;
import modelo.Celular;
import modelo.Marca;

/**
 *
 * @author ghuaba
 */
public class MarcaDAO extends AdaptadorDAO<Marca> {

    private Marca marca;

    public MarcaDAO() {
        super(Marca.class);
    }

    public Marca getMarca() {
        if (this.marca == null) {
            this.marca = new Marca();
        }
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public void guardar() throws IOException {
        marca.setId(generateID());
        this.guardar(marca);
    }

    public void modificar(Integer pos) throws VacioException, PosicionException, IOException {
        this.modificar(marca, pos);
    }

    private Integer generateID() {
        return listar().size() + 1;
    }

    
    //si funciona  y lo hace array lo evalua y luego lo devuelve en lista
    public ListaEnlazada<Marca> ordenarMarcaNombre(ListaEnlazada<Marca> lista) {
        try {
            Marca[] matriz = lista.toArray();
            for (int i = 1; i < lista.size(); i++) {
                Marca key = lista.get(i);
                int j = i - 1;
                while (j >= 0 && (matriz[j].getNombre().compareToIgnoreCase(key.getNombre())) > 0) {
                    //lista.update(j+1, lista.get(j));
                    matriz[j + 1] = matriz[j];
                    j = j - 1;
                }
                //lista.update(j+1, key);
                matriz[j + 1] = key;
            }
            lista.toList(matriz);
        } catch (Exception e) {
        }
        return lista;
    }

    
    //metodo en array
    public ListaEnlazada<Marca> ordenarMarcaArray(ListaEnlazada<Marca> lista) {
        try {
            Marca[] matriz = lista.toArray();
            for (int i = 1; i < lista.size(); i++) {
                Marca key = matriz[i];  //cambioooooooooooo aqui a diferebciua dek de lista
                int j = i - 1;
                while (j >= 0 && (matriz[j].getNombre().compareToIgnoreCase(key.getNombre())) > 0) {
                    //lista.update(j+1, lista.get(j));
                    matriz[j + 1] = matriz[j];
                    j = j - 1;
                }
                //lista.update(j+1, key);
                matriz[j + 1] = key;
            }
            lista.toList(matriz);
        } catch (Exception e) {
        }
        return lista;
    }
            
    

    public Marca buscarNombre(String dato) throws Exception {
        Marca resultado = null;
        ListaEnlazada<Marca> lista = listar();
        for (int i = 0; i < lista.size(); i++) {
            Marca aux = lista.get(i);

            if (aux.getNombre().toLowerCase().equals(dato.toLowerCase())) {
                resultado = aux;
                break;
            }
        }
        return resultado;
    }
    
    
    public Marca buscarMarcaBinaria(String atributo) throws Exception {
    ListaEnlazada<Marca> listaMarcas = listar();
    listaMarcas = ordenarMarcaNombre(listaMarcas);
    
    int left = 0;
    int right = listaMarcas.size() - 1;
    
    while (left <= right) {
        int mid = left + (right - left) / 2;
        Marca marcaActual = listaMarcas.get(mid);
        
        int comparacion = marcaActual.getNombre().compareToIgnoreCase(atributo);
        
        if (comparacion == 0) {
            return marcaActual;
        } else if (comparacion < 0) {
            left = mid + 1;
        } else {
            right = mid - 1;
        }
    }
    
    return null;
}
    
      public ListaEnlazada<Celular> buscarCelularesPorMarca(String nombreMarca) throws Exception {
    ListaEnlazada<Celular> resultados = new ListaEnlazada<>();

    MarcaDAO marcaDao = new MarcaDAO();
    Marca marcaBuscada = marcaDao.buscarMarcaBinaria(nombreMarca.trim());

    if (marcaBuscada != null) {
        CelularDAO celularDao = new CelularDAO();
        ListaEnlazada<Celular> listaCelulares = celularDao.listar();

        NodoLista<Celular> nodoActual = listaCelulares.getCabecera();
        while (nodoActual != null) {
            Celular celular = nodoActual.getInfo();
            if (celular.getId_marca().intValue() == marcaBuscada.getId().intValue()) {
                resultados.insertar(celular);
                break;
            }
            nodoActual = nodoActual.getSig();
        }
    }

    return resultados;
}

}
