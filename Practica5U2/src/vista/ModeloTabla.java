/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista;

import controlador.dao.MarcaDAO;
import controlador.dao.PaisFabricacionDAO;
import controlador.lista.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import modelo.Celular;
import modelo.Marca;
import modelo.PaisFabricacion;

/**
 *
 * @author ghuaba
 */
public class ModeloTabla extends AbstractTableModel{

   //private SucursalDao datos = new SucursalDao();
    private ListaEnlazada<Celular> lista = new ListaEnlazada<>();    


    
    
    
    
    public ListaEnlazada<Celular> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<Celular> lista) {
        this.lista = lista;
    }

    
    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public int getRowCount() {
        return lista.size();
    }

    @Override
    public String getColumnName(int i) {
        
        switch(i) {
            case 0: return "Nro";   
            case 1: return "Modelo";
            case 2: return "IdDispositivo";
            case 3: return "Precio";        
            case 4: return "Año";   
            case 5: return "Marca";
            case 6: return "PaisFabricacion";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Celular s = null;
        try {
            s = lista.get(i);
        } catch (Exception e) {
        }
        switch(i1) {
            case 0: return (i+1);
            case 1: return (s != null)? s.getModelo():"NO DEFINIDO";
            case 2: return (s != null)? s.getId_dispositivo():"NO DEFINIDO";
            case 3: return (s != null)? "$" +s.getPrecio():"$0.0";
            case 4: return (s != null)? s.getAnio():"NO DEFINIDO";
            case 5: return (s != null)? new MarcaDAO().obtener(s.getId_marca()):"NO DEFINIDO";
            case 6: return (s != null)? new PaisFabricacionDAO().obtener(s.getId_paisFabriacion()):"NO DEFINIDO";
            
   
            default: return null;
        }
    }
}

    

