/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.utilidades;

import controlador.dao.MarcaDAO;
import controlador.dao.PaisFabricacionDAO;
import controlador.lista.ListaEnlazada;
import javax.swing.JComboBox;
import modelo.Marca;
import modelo.PaisFabricacion;

/**
 *
 * @author ghuaba
 */
public class Utilidades {
       public static void  loadMarca(JComboBox cbx,MarcaDAO md)throws Exception{
        cbx.removeAllItems();
        ListaEnlazada<Marca> lista = md.listar();
        for (int i = 0; i < md.listar().size(); i++) {
            cbx.addItem(lista.get(i).getNombre());
        }
    }
       public static void  loadPais(JComboBox cbx,PaisFabricacionDAO paisD)throws Exception{
        cbx.removeAllItems();
        ListaEnlazada<PaisFabricacion> lista = paisD.listar();
        for (int i = 0; i < paisD.listar().size(); i++) {
            cbx.addItem(lista.get(i).getNombre());
        }
    }
}
