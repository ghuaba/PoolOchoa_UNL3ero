/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package vista;

import controlador.dao.CelularDAO;
import controlador.dao.MarcaDAO;

import controlador.dao.PaisFabricacionDAO;

import controlador.lista.ListaEnlazada;
import controlador.busqueda.Binaria;
import controlador.busqueda.BinaryLineal;
import java.util.Calendar;

import javax.swing.JOptionPane;
import modelo.Celular;
import vista.utilidades.Utilidades;

/**
 *
 * @author ghuaba
 */
public class FrmPrincipal extends javax.swing.JDialog {

    private ModeloTabla modelo = new ModeloTabla();
    private CelularDAO celDao = new CelularDAO();
    private MarcaDAO marDao = new MarcaDAO();
    private PaisFabricacionDAO paisDao = new PaisFabricacionDAO();
    private Integer fila = -1;
    ListaEnlazada<Celular> lista = new ListaEnlazada<>();
    Binaria binary = new Binaria();
    BinaryLineal binaryLineal = new BinaryLineal();

    private void loadTable() {
        modelo.setLista(celDao.listar());
        tblTabla.setModel(modelo);
        tblTabla.updateUI();
    }

    private void limpiar() {
        jsAnio.setValue(Calendar.getInstance().get(Calendar.YEAR));
        txtModelo.setText("");
        txtId.setText("");
        txtValor.setText("0.0");
        loadTable();
        loadComboMarca();
        loadComboPais();
        celDao.setCelular(null);
        fila = -1;
        tblTabla.clearSelection();
        txtBuscar.setText("");
        cbxCriterio.setSelectedIndex(0);
    }

    private void loadComboMarca() {
        try {
            Utilidades.loadMarca(cbxMarca, marDao);
        } catch (Exception e) {
        }

    }

    private void loadComboPais() {
        try {
            Utilidades.loadPais(cbxPais, paisDao);
        } catch (Exception e) {
        }

    }

    private void save() {
        if (jsAnio.getValue().toString().isEmpty() || txtModelo.getText().trim().isEmpty()
                || txtId.getText().trim().isEmpty() || txtValor.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "Ingrese todos los datos correspondientes ", "ERROR", JOptionPane.ERROR_MESSAGE);
        } else {
            try {
                celDao.getCelular().setPrecio(Double.parseDouble(txtValor.getText()));
                celDao.getCelular().setAnio(Integer.parseInt(jsAnio.getValue().toString()));
                celDao.getCelular().setModelo(txtModelo.getText());
                celDao.getCelular().setId_dispositivo(txtId.getText());
                celDao.getCelular().setId_marca(marDao.buscarNombre(cbxMarca.getSelectedItem().toString()).getId());
                celDao.getCelular().setId_paisFabricacion(paisDao.buscarNombre(cbxPais.getSelectedItem().toString()).getId());
                if (celDao.getCelular().getId() != null) {
                    celDao.modificar(fila);
                } else {
                    celDao.guardar();
                }
                limpiar();
                JOptionPane.showMessageDialog(null, "Se ha guardado ", "Message", JOptionPane.INFORMATION_MESSAGE);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void search() {
    String criterio = cbxCriterio.getSelectedItem().toString();
    String tipoBusqueda = cbxTipoBusqueda.getSelectedItem().toString();
    String texto = txtBuscar.getText();
    try {
        ListaEnlazada<Celular> lista = new ListaEnlazada<>();

        switch (criterio) {
            case "Modelo":
                if (tipoBusqueda.equalsIgnoreCase("BinariaLineal")) {
                    lista = binaryLineal.binaryLinealCelular(texto);
                } else if (tipoBusqueda.equalsIgnoreCase("Binaria")) {
                    lista = binary.binarySearchCelular(texto);
                }
                break;
            case "IdDispositivo":
                if (tipoBusqueda.equalsIgnoreCase("BinariaLineal")) {
                    lista = binaryLineal.binaryLinealId(texto);
                } else if (tipoBusqueda.equalsIgnoreCase("Binaria")) {
                    lista = binary.binarySearchId(texto);
                }
                break;
            case "PaisFabricacion":
                if (tipoBusqueda.equalsIgnoreCase("BinariaLineal")) {
                    lista = celDao.searchLinealPais(texto);
                } else if (tipoBusqueda.equalsIgnoreCase("Binaria")) {
                    lista = binary.binarySearchPais(texto);
                }
                break;
            case "Marca":
                if (tipoBusqueda.equalsIgnoreCase("BinariaLineal")) {
                    lista = celDao.searchLinealMarca(texto);
                } else if (tipoBusqueda.equalsIgnoreCase("Binaria")) {
                    lista = binary.binarySearchMarca(texto);
                }
                break;
        }

        modelo.setLista(lista);
        tblTabla.setModel(modelo);
        tblTabla.updateUI();
    } catch (Exception e) {
        JOptionPane.showMessageDialog(null, "No se encuentra esa búsqueda", "ERROR", JOptionPane.ERROR_MESSAGE);
    }
}

    

//    private void search() {
//        String criterio = cbxCriterio.getSelectedItem().toString();
//        String tipoBusqueda = cbxTipoBusqueda.getSelectedItem().toString();
//        String texto = txtBuscar.getText();
//        try {
//            if (criterio.equalsIgnoreCase("Modelo") && tipoBusqueda.equalsIgnoreCase("BinariaLineal")) {
//                modelo.setLista(binaryLineal.binaryLinealCelular(texto));
//            }
//            if (criterio.equalsIgnoreCase("Modelo") && tipoBusqueda.equalsIgnoreCase("Binaria")) {
//                modelo.setLista(binary.binarySearchCelular(texto));
//            }
//
//            if (criterio.equalsIgnoreCase("IdDispositivo") && tipoBusqueda.equalsIgnoreCase("BinariaLineal")) {
//                modelo.setLista(celDao.searchLinealMarca(texto));
//            }
//            if (criterio.equalsIgnoreCase("IdDispositivo") && tipoBusqueda.equalsIgnoreCase("Binaria")) {
//                modelo.setLista(binary.binarySearchId(texto));
//            }
//
//            if (criterio.equalsIgnoreCase("PaisFabricacion") && tipoBusqueda.equalsIgnoreCase("BinariaLineal")) {
//                modelo.setLista(celDao.searchLinealPais(texto));
//            }
//            if (criterio.equalsIgnoreCase("PaisFabricacion") && tipoBusqueda.equalsIgnoreCase("Binaria")) {
//                modelo.setLista(binary.binarySearchPais(texto));
//            }
//            if (criterio.equalsIgnoreCase("Marca") && tipoBusqueda.equalsIgnoreCase("BinariaLineal")) {
//                modelo.setLista(celDao.searchLinealMarca(texto));
//            }
//            if (criterio.equalsIgnoreCase("Marca") && tipoBusqueda.equalsIgnoreCase("Binaria")) {
//                modelo.setLista(binary.binarySearchMarca(texto));
//            }
//
//            tblTabla.setModel(modelo);
//            tblTabla.updateUI();
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null, "No se encuentra esa busqueda", "ERROR", JOptionPane.ERROR_MESSAGE);
//        }
//    }

    /**
     * Creates new form FrmPrincipal
     */
    public FrmPrincipal(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        limpiar();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtModelo = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtId = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtValor = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jsAnio = new javax.swing.JSpinner();
        jLabel5 = new javax.swing.JLabel();
        cbxMarca = new javax.swing.JComboBox<>();
        btnSave = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        cbxPais = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        cbxCriterio = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        txtBuscar = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        cbxTipoBusqueda = new javax.swing.JComboBox<>();
        jButton3 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTabla = new javax.swing.JTable();
        btnSeleccionar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Informacion Celular"));
        jPanel1.setLayout(null);

        jLabel1.setText("Modelo:");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(40, 30, 67, 20);

        txtModelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtModeloActionPerformed(evt);
            }
        });
        jPanel1.add(txtModelo);
        txtModelo.setBounds(140, 30, 120, 26);

        jLabel2.setText("IdDispositivo:");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(400, 30, 90, 20);
        jPanel1.add(txtId);
        txtId.setBounds(520, 30, 180, 30);

        jLabel3.setText("ValorPrecio:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(30, 70, 80, 20);
        jPanel1.add(txtValor);
        txtValor.setBounds(140, 70, 120, 26);

        jLabel4.setText("Anio:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(420, 70, 40, 20);
        jPanel1.add(jsAnio);
        jsAnio.setBounds(520, 70, 100, 26);

        jLabel5.setText("MarcaPerteneciente:");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(40, 110, 140, 20);

        cbxMarca.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel1.add(cbxMarca);
        cbxMarca.setBounds(190, 110, 130, 26);

        btnSave.setText("Guardar");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        jPanel1.add(btnSave);
        btnSave.setBounds(590, 150, 100, 30);

        btnLimpiar.setText("Cancelar/limpiar celdas");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        jPanel1.add(btnLimpiar);
        btnLimpiar.setBounds(520, 110, 180, 26);

        jLabel9.setText("PaisFabricacion:");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(40, 150, 130, 20);

        cbxPais.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel1.add(cbxPais);
        cbxPais.setBounds(190, 150, 140, 26);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(20, 20, 720, 200);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Opciones de busqueda"));
        jPanel2.setLayout(null);

        jLabel6.setText("Columnas Criterios:");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(50, 30, 150, 20);

        cbxCriterio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Modelo", "PaisFabricacion", "Marca", "IdDispositivo" }));
        jPanel2.add(cbxCriterio);
        cbxCriterio.setBounds(370, 20, 170, 26);

        jLabel7.setText("Ingrese la palabra que desea buscar:");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(50, 70, 250, 20);

        txtBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtBuscarActionPerformed(evt);
            }
        });
        jPanel2.add(txtBuscar);
        txtBuscar.setBounds(370, 70, 140, 26);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Eleccion busqueda"));
        jPanel4.setLayout(null);

        jLabel8.setText("Tipo de busqueda:");
        jPanel4.add(jLabel8);
        jLabel8.setBounds(80, 30, 150, 20);

        cbxTipoBusqueda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Binaria", "BinariaLineal" }));
        jPanel4.add(cbxTipoBusqueda);
        cbxTipoBusqueda.setBounds(260, 30, 130, 26);

        jButton3.setText("Buscar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel4.add(jButton3);
        jButton3.setBounds(540, 30, 82, 26);

        jPanel2.add(jPanel4);
        jPanel4.setBounds(20, 110, 680, 80);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(20, 230, 720, 210);

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Tabla datos Celular"));
        jPanel5.setLayout(null);

        tblTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblTabla);

        jPanel5.add(jScrollPane1);
        jScrollPane1.setBounds(40, 30, 650, 150);

        btnSeleccionar.setText("SELECCIONAR");
        btnSeleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarActionPerformed(evt);
            }
        });
        jPanel5.add(btnSeleccionar);
        btnSeleccionar.setBounds(530, 200, 160, 26);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
        );

        jPanel5.add(jPanel3);
        jPanel3.setBounds(830, -20, 120, 50);

        getContentPane().add(jPanel5);
        jPanel5.setBounds(20, 440, 720, 230);

        setSize(new java.awt.Dimension(767, 716));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void txtModeloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtModeloActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtModeloActionPerformed

    private void txtBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtBuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtBuscarActionPerformed

    private void btnSeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarActionPerformed
        fila = tblTabla.getSelectedRow();
        if (fila >= 0) {
            try {
                Integer id = modelo.getLista().get(fila).getId();
                celDao.setCelular(celDao.obtener(id));
                jsAnio.setValue(celDao.getCelular().getAnio());
                txtModelo.setText(celDao.getCelular().getModelo());
                txtId.setText(celDao.getCelular().getId_dispositivo());
                txtValor.setText(String.valueOf((celDao.getCelular().getPrecio())));
                cbxMarca.setSelectedItem(marDao.obtener(celDao.getCelular().getId_marca()).getNombre().toUpperCase());
                cbxPais.setSelectedItem(paisDao.obtener(celDao.getCelular().getId_paisFabriacion()).getNombre().toUpperCase());
            } catch (Exception e) {
            }

        } else {
            JOptionPane.showMessageDialog(null, "Escoja un registro de la tabla", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnSeleccionarActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        save();
    }//GEN-LAST:event_btnSaveActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        search();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmPrincipal dialog = new FrmPrincipal(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSeleccionar;
    private javax.swing.JComboBox<String> cbxCriterio;
    private javax.swing.JComboBox<String> cbxMarca;
    private javax.swing.JComboBox<String> cbxPais;
    private javax.swing.JComboBox<String> cbxTipoBusqueda;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner jsAnio;
    private javax.swing.JTable tblTabla;
    private javax.swing.JTextField txtBuscar;
    private javax.swing.JTextField txtId;
    private javax.swing.JTextField txtModelo;
    private javax.swing.JTextField txtValor;
    // End of variables declaration//GEN-END:variables
}
