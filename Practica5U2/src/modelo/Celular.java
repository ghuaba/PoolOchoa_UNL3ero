/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.util.Objects;

/**
 *
 * @author ghuaba
 */
public class Celular {

    private Integer id;
    private String modelo;
    private Double precio;
    //private String placa;
    private String id_dispositivo;   //unico como si se tratara de una placa
    private Integer anio;
    private Integer id_marca;
    private Integer id_paisFabricacion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public String getId_dispositivo() {
        return id_dispositivo;
    }

    public void setId_dispositivo(String id_dispositivo) {
        this.id_dispositivo = id_dispositivo;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Integer getId_marca() {
        return id_marca;
    }

    public void setId_marca(Integer id_marca) {
        this.id_marca = id_marca;
    }

    public Integer getId_paisFabriacion() {
        return id_paisFabricacion;
    }

    public void setId_paisFabricacion(Integer id_paisFabricacion) {
        this.id_paisFabricacion = id_paisFabricacion;
    }

    /*
     @Override
    public String toString() {
        return id + " "+modelo;
    }
     */
    @Override
    public String toString() {
        return modelo.toString();
    }
}
