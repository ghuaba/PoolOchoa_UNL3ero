/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.exception.EspacioException;
import modelo.EnumMes;
import modelo.Sucursal;
import modelo.Venta;

/**
 *
 * @author ghuaba
 */
public class SucursalControl {

    private ListaEnlazada<Sucursal> sucursales;
    private Sucursal sucursal;
    private Venta venta;
    
    private int id = 0;

    private ListaEnlazada<Venta> listaVentas = new ListaEnlazada<>();
    
    public SucursalControl() {
        sucursales = new ListaEnlazada<>();
        if(listaVentas.isEmpty()){
            this.listaVentas = new ListaEnlazada<>();
            for(EnumMes mes : EnumMes.values()){
                this.venta = new Venta();
                getVenta().setMes(mes);
                getVenta().setValor(0.0);
                getVenta().setId(id);
                id++;
                listaVentas.insertar(venta);
            }
        }
    }

    public boolean guardarVentas(Integer posVenta, Double valor) throws EspacioException, VacioException, PosicionException {
        if (this.sucursal != null) {
            if (posVenta <= this.sucursal.getLsVenta().size() - 1) {
                //sucursal.getVentas()[posVenta].setValor(valor);
                sucursal.getLsVenta().get(posVenta).setValor(valor);
            } else {
                throw new EspacioException();
            }
        } else {
            throw new NullPointerException("Debe seleccionar una sucursal: ");
        }
        return true;
    }

    public Venta getVenta() {
        if (venta == null) {
            venta = new Venta(); }
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

    public Sucursal getSucursal() {
        if (sucursal == null) {
            sucursal = new Sucursal();
        }
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }
    /**
     * @return the sucursales
     */
    public ListaEnlazada<Sucursal> getSucursales() {
        return sucursales;
    }
    /**
     * @param sucursales the sucursales to set
     */
    public void setSucursales(ListaEnlazada<Sucursal> sucursales) {
        this.sucursales = sucursales;
    }

    /**
     * @return the listaVentas
     */
    public ListaEnlazada<Venta> getListaVentas() {
        return listaVentas;
    }

    /**
     * @param listaVentas the listaVentas to set
     */
    public void setListaVentas(ListaEnlazada<Venta> listaVentas) {
        this.listaVentas = listaVentas;
    }
    
     public void inicializarVentas() {
        if (listaVentas.isEmpty()) {
            this.listaVentas = new ListaEnlazada<>();
            for (EnumMes mes : EnumMes.values()) {
                this.venta = new Venta();
                this.venta.setMes(mes);
                this.venta.setValor(0.0);
                this.venta.setId(id);
                id++;
                listaVentas.insertar(venta);
            }
        }
    }    
}
    
    


