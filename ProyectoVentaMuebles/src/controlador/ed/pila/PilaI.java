/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ed.pila;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.ed.pila.Exception.TopeException;

/**
 *
 * @author ghuaba
 */
public class PilaI<E> extends ListaEnlazada<E> {
    private Integer cima;

    public PilaI() {
    }

    public PilaI(Integer cima) {
        this.cima = cima;
    }
    
    public boolean isFull(){
        return (size() >= getCima());
    }

    public void push (E info)throws TopeException{
        if (!isFull())
            insertarInicio(info);
        else
            throw new TopeException();
    }
    
    
    //metodo de borrar
    public E pop() throws VacioException, PosicionException{
        E dato = null;
        if (isEmpty())
            throw new VacioException();
        else
            return delete(0);
    }
    
    /**
     * @return the cima
     */
    public Integer getCima() {
        return cima;
    }

    /**
     * @param cima the cima to set
     */
    public void setCima(Integer cima) {
        this.cima = cima;
    }
    
    
    
}
