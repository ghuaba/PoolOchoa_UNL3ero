/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.ed.pila;

import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.ed.pila.Exception.TopeException;

/**
 *
 * @author ghuaba
 */
public class Pila<E> {
    private PilaI<E> pilaI;
    
    public Pila(Integer cima) {
        this.pilaI = new PilaI<>(cima);
    }
    
    public Integer getCima(){
        return pilaI.getCima();
    }
    
    public void push(E obj) throws TopeException{
        pilaI.push(obj);
    }
    
    public E pop() throws VacioException, PosicionException{
        return pilaI.pop(); 
    }
    
    public void print() throws VacioException{
        pilaI.imprimir();
    }

    /**
     * @return the pilaI
     */
    public PilaI<E> getPilaI() {
        return pilaI;
    }

    /**
     * @param pilaI the pilaI to set
     */
    public void setPilaI(PilaI<E> pilaI) {
        this.pilaI = pilaI;
    }
    
    
}
