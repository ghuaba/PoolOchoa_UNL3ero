/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.DAO.Interfaz;

import controlador.ed.lista.exception.VacioException;
import controlador.ed.pila.Exception.TopeException;
import controlador.ed.pila.Pila;
import java.io.IOException;

/**
 *
 * @author ghuaba
 * @param <E>
 */
public interface InterfazDaoPila<E> {
  

    public void push(E obj) throws IOException, TopeException;

    public Pila<E> listar();

    public E obtener(Integer id);
 
    public void pop() throws VacioException;
   
    public void deleteFirst() throws VacioException;
    
}
