/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.DAO.Interfaz;

import controlador.ed.cola.Cola;
import controlador.ed.lista.exception.VacioException;
import controlador.ed.pila.Exception.TopeException;
import java.io.IOException;

/**
 *
 * @author ghuaba
 */
public interface InterfazDaoCola<E> {
    
public void queue(E obj) throws IOException, TopeException;

    public Cola<E> listar();

    public void dequeue() throws VacioException;
    
    public E obtener(Integer id);
    
    public void saveCola(Cola<E> cola) throws IOException;
}

