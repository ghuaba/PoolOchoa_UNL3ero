package controlador.DAO;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.jboss.logging.Logger;
import controlador.DAO.Interfaz.InterfazDaoLista;

/**
 *
 * @author ghuaba
 */
public class AdaptadorDao<T> implements InterfazDaoLista<T> {

    private Conexion conexion;
    private Class clazz;
    public String url;

    public AdaptadorDao(Class clazz) {
        this.conexion = new Conexion();
        this.clazz = clazz;
        this.url = Conexion.URL + clazz.getSimpleName().toLowerCase() + ".json";
    }

    @Override
    public void guardar(T obj) throws IOException {

        ListaEnlazada<T> lista = listar();
        lista.insertar(obj);

        conexion.getXstream().alias(lista.getClass().getName(), ListaEnlazada.class);
        conexion.getXstream().toXML(lista, new FileWriter(url));
    }

    @Override
    public void modificar(T obj, Integer pos) {
        ListaEnlazada<T> lista = listar();
        try {
            lista.modificar(obj, pos);
            conexion.getXstream().alias(lista.getClass().getName(), ListaEnlazada.class);
            conexion.getXstream().toXML(lista, new FileWriter(url));
        } catch (PosicionException | IOException ex) {
            Logger.getLogger(ex.getMessage());
        }
    }

    @Override
    public ListaEnlazada listar() {
        ListaEnlazada<T> lista = new ListaEnlazada<>();

        try {

            lista = (ListaEnlazada<T>) conexion.getXstream().fromXML(new File(url));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return lista;
    }

    @Override
    public T obtener(Integer id) {
        ListaEnlazada<T> lista = listar();
        return (T) lista;
    }

}
