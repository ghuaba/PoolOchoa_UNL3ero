/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.DAO;

import controlador.DAO.Interfaz.InterfazDaoPila;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.ed.pila.Exception.TopeException;
import controlador.ed.pila.Pila;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Logger;

/**
 *
 * @author ghuaba
 */
public class AdaptadorDaoHistory<E> implements InterfazDaoPila<E> {

    private Conexion conexion;
    private Class clazz;
    private String url;

    public AdaptadorDaoHistory(Class clazz) {
        this.conexion = new Conexion();
        this.clazz = clazz;
        this.url = Conexion.URL + clazz.getSimpleName().toLowerCase() + ".json";
    }

    @Override
    public Pila<E> listar() {
        Pila<E> pila = new Pila<>(20);
        try {
            pila = (Pila<E>) conexion.getXstream().fromXML(new File(url));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return pila;
    }

    @Override
    public E obtener(Integer id) {
        Pila<E> pila = listar();
        return (E) pila;
    }

    @Override
    public void push(E obj) throws IOException, TopeException {
        Pila<E> pila = listar();
        pila.push(obj);
        conexion.getXstream().alias(pila.getClass().getName(), Pila.class);
        conexion.getXstream().toXML(pila, new FileWriter(url));
    }

    @Override
    public void pop() throws VacioException {
        Pila<E> pila = listar();
        try {
            pila.pop();
            conexion.getXstream().alias(pila.getClass().getName(), Pila.class);
            conexion.getXstream().toXML(pila, new FileWriter(url));
        } catch (PosicionException | IOException ex) {
            Logger.getLogger(ex.getMessage());
        }
    }

    @Override
    public void deleteFirst() throws VacioException {
        Pila<E> pila = listar();
        try {
            Pila<E> tempoPila = new Pila<>(20);

            while (!pila.getPilaI().isEmpty()) {
                tempoPila.push(pila.pop());
            }

            tempoPila.pop();

            while (!tempoPila.getPilaI().isEmpty()) {
                pila.push(tempoPila.pop());
            }

            conexion.getXstream().alias(pila.getClass().getName(), Pila.class);
            conexion.getXstream().toXML(pila, new FileWriter(url));
        } catch (VacioException | IOException | TopeException | PosicionException ex) {
            Logger.getLogger(ex.getMessage());
        }
    }

}
