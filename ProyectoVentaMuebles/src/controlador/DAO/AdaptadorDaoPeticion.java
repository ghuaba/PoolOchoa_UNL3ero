/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.DAO;

import controlador.DAO.Interfaz.InterfazDaoCola;
import controlador.ed.cola.Cola;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.ed.pila.Exception.TopeException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Logger;
import modelo.Peticion;

/**
 *
 * @author ghuaba
 */
public class AdaptadorDaoPeticion <E> implements InterfazDaoCola<E>{

    private Conexion conexion;
    private Class clazz;
    private String url;
    private long borrarPeticiones = 3600;
    private int tope = 10;
    
    public AdaptadorDaoPeticion(Class clazz) {
        this.conexion = new Conexion();
        this.clazz = clazz;
        this.url = Conexion.URL + clazz.getSimpleName().toLowerCase() + ".json";
    }

    @Override
    public void queue(E obj) throws IOException, TopeException {
        Cola<E> cola = listar();
        cola.queue(obj);
        conexion.getXstream().alias(cola.getClass().getName(), Cola.class);
        conexion.getXstream().toXML(cola, new FileWriter(url));
    }
     @Override
    public void dequeue() throws VacioException {
        Cola<E> cola = listar();
        try {
            cola.dequeue();
            conexion.getXstream().alias(cola.getClass().getName(), Cola.class);
            conexion.getXstream().toXML(cola, new FileWriter(url));
        } catch (PosicionException | IOException ex) {
            Logger.getLogger(ex.getMessage());
        }
    }

    @Override
    public Cola<E> listar() {
        Cola<E> cola = new Cola<>(tope);
        try {
            cola = (Cola<E>) conexion.getXstream().fromXML(new File(url));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return cola;
    }

    @Override
    public E obtener(Integer id) {
        Cola<E> cola = listar();
        return (E) cola;
    }
    
    public void saveCola(Cola<E> cola) throws IOException {
        conexion.getXstream().alias(cola.getClass().getName(), Cola.class);
        conexion.getXstream().toXML(cola, new FileWriter(url));
    }

    public void deletePeticion() throws PosicionException, VacioException, TopeException, IOException {
        Cola<Peticion> colaPeticion = (Cola<Peticion>) listar();
        Date fechaActual = new Date();
        Cola<Peticion> newColaPeticion = new Cola<>(tope);
        while (!colaPeticion.getCola().isFull()) {
            Peticion peticion = colaPeticion.dequeue();
            long dTiempo = fechaActual.getTime() - peticion.getFecha().getTime();
            if (dTiempo < borrarPeticiones) {
                newColaPeticion.queue(peticion);
            }
        }
        saveCola((Cola<E>) newColaPeticion);
    }

    
}

