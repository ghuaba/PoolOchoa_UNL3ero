/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;

import controlador.DAO.AdaptadorDaoHistory;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.ed.pila.Exception.TopeException;
import java.io.IOException;
import java.util.Date;
import modelo.Historial;
import modelo.Venta;

/**
 *
 * @author ghuaba
 */
public class HistorialControl {
    
        private AdaptadorDaoHistory<Historial> daoH = new AdaptadorDaoHistory<>(Historial.class);

    public HistorialControl() {
    }

    
      public void addHistory(Venta v) throws VacioException, TopeException, PosicionException, IOException {
        Historial nuevaInfo = new Historial();
        nuevaInfo.setVenta(v);
        nuevaInfo.setFecha(new Date());

        if (daoH.listar().getPilaI().isFull()) {
            daoH.deleteFirst();
        }
        
        daoH.push(nuevaInfo);
    }
}
