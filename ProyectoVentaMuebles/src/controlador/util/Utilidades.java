/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.util;


import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.NodoLista;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import modelo.Sucursal;
import modelo.Venta;

/**
 *
 * @author ghuaba
 */
public class Utilidades {

//    public static void imprimir(Object[] objs) {
//        System.out.println("LIsta de" + objs.getClass().getSimpleName());
//        for (Object o : objs) {
//            System.out.println(o.toString());
//        }
//    }

     public static Double sumarVentas(Sucursal s) {
        double plus = 0.0;
        ListaEnlazada<Venta> listaVentas = s.getLsVenta();

        NodoLista<Venta> nodo = listaVentas.getCabecera();
        while (nodo != null) {
            Venta venta = nodo.getInfo();
            plus += venta.getValor();
            nodo = nodo.getSig();
        }

        return plus;
    }

    
    
     public static Double mediaVentas(Sucursal s) {
        Double plus = sumarVentas(s);
        ListaEnlazada<Venta> lsVentas = s.getLsVenta();
        if (plus == 0) {
            return plus;
        } else {
            return plus / lsVentas.size();
        }
    }
    
    
       public static String ventaMenor(Sucursal s)throws PosicionException, VacioException{
 ListaEnlazada<Venta> listaVentas = s.getLsVenta();   
 Double ventas = ventaMayor(s);
        NodoLista<Venta> nodo = listaVentas.getCabecera();
        String menorVenta = "";
        
        while (nodo != null){
            Venta v = nodo.getInfo();
            if (ventas > v.getValor()){
                ventas = v.getValor();
                menorVenta = v.getMes().name();
            }
            nodo = nodo.getSig();
        }
        
        return menorVenta;
    }
    
    
    public static Double ventaMayor(Sucursal s)throws PosicionException, VacioException{
        ListaEnlazada<Venta> listaVentas = s.getLsVenta();
        NodoLista<Venta> nodo = listaVentas.getCabecera();
        Double venta = 0.0;
        while (nodo != null){
            Venta v = nodo.getInfo();
            if(v.getValor() > venta)
                venta = v.getValor();
            nodo = nodo.getSig();
        }
        return venta;
        
    }
    
    
      
    
}
