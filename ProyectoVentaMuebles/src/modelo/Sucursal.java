/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;

/**
 *
 * @author ghuaba
 */
public class Sucursal {

    private Integer id;
    private String nombre;
    private ListaEnlazada<Venta> lsVenta ;

    public Sucursal() {
        this.lsVenta = new ListaEnlazada<Venta>();
    }
  public Sucursal(Integer id, String nombre, ListaEnlazada<Venta> lsVenta) {
        this.id = id;
        this.nombre = nombre;
        this.lsVenta = lsVenta;
    }
    
    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return getNombre().toString() + " " + getId();
    }


    public ListaEnlazada<Venta> getLsVenta() {
        return lsVenta;
    }
    public void setLsVenta(ListaEnlazada<Venta> lsVenta) {
        this.lsVenta = lsVenta;
    }
    
    
    public void setVenta(Venta venta) {
        lsVenta.insertar(venta);
    }

    public Venta getVenta(int posicion) {
        try {
            return lsVenta.get(posicion);
        } catch (PosicionException | VacioException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

}
