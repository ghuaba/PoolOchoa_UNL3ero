/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;

import java.util.Date;

/**
 *
 * @author ghuaba
 */
public class Peticion {
    
private String peticion;
    private Date fecha;

    public Peticion() {
    }

    public Peticion(String peticion, Date fecha) {
        this.peticion = peticion;
        this.fecha = fecha;
    }

    @Override
    public String toString() {
        return super.toString(); 
    }

    public String getPeticion() {
        return peticion;
    }


    public void setPeticion(String peticion) {
        this.peticion = peticion;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
}
