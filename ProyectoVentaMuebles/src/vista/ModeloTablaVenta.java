/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import javax.swing.table.AbstractTableModel;
import modelo.Venta;

/**
 *
 * @author ghuaba
 */
public class ModeloTablaVenta extends AbstractTableModel {

     private ListaEnlazada<Venta> lsVentas = new ListaEnlazada<>();
    private int columnas = 2;
    private int meses = 12;


    public ListaEnlazada<Venta> getLsVentas() {
        return lsVentas;
    }

    /**
     * @param datos the datos to set
     */
    public void setLsVentas(ListaEnlazada<Venta> lsVentas) {
        this.lsVentas = lsVentas;
    }
    
@Override
    public int getRowCount() {
        return meses;
    }
    
    @Override
    public int getColumnCount() {
        return columnas;
    }


    @Override
    public Object getValueAt(int i, int i1) {

        try {
            Venta s = lsVentas.get(i);
            switch (i1) {
                case 0:
                    return (s != null) ? s.getMes() : "NO DEFINIDO";
                case 1:
                    return (s != null) ? s.getValor() : 0.0;
                default:
                    return null;
            }

        } catch (PosicionException | VacioException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }

        @Override
        public String getColumnName(int column) {
        switch (column) {
                case 0:
                    return "Mes";
                case 1:
                    return "Ventas Anual";
                default:
                    return null;
            }

        }
   
}