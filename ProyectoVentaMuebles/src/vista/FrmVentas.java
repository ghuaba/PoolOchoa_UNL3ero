package vista;

import controlador.DAO.AdaptadorDao;
import controlador.HistorialControl;
import controlador.SucursalControl;
import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.ed.pila.Exception.TopeException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modelo.Sucursal;
import modelo.Venta;

/**
 *
 * @author ghuaba
 */
public class FrmVentas extends javax.swing.JDialog {

    private ModeloTablaVenta modelo = new ModeloTablaVenta();
    private ListaEnlazada<Venta> listaV = new ListaEnlazada<>();
    private SucursalControl control;
    private Sucursal sucursal;
    // private int row = -1;
    private ListaEnlazada<Venta> listaVentas = new ListaEnlazada<>();

    private AdaptadorDao<Sucursal> dao = new AdaptadorDao<>(Sucursal.class);

   
    private Venta venta;
    private int fila = -1;
    private int id = 0;

    public FrmVentas(java.awt.Frame parent, boolean modal, Sucursal sc) {
        super(parent, modal);
        initComponents();
        this.sucursal = sc;
        this.listaV = sucursal.getLsVenta();
        lblSucursal.setText(sucursal.getNombre());
        cargarTabla();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblMes = new javax.swing.JLabel();
        txtValor = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblSucursal = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTabla = new javax.swing.JTable();
        btnSeleccionar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Ventas"));
        jPanel1.setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel2.setLayout(null);

        jLabel2.setFont(new java.awt.Font("Liberation Sans", 0, 18)); // NOI18N
        jLabel2.setText("Mes:");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(50, 20, 60, 30);

        jLabel3.setFont(new java.awt.Font("Liberation Sans", 0, 18)); // NOI18N
        jLabel3.setText("Valor:");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(310, 20, 70, 30);
        jPanel2.add(lblMes);
        lblMes.setBounds(100, 20, 150, 30);
        jPanel2.add(txtValor);
        txtValor.setBounds(410, 20, 140, 28);

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        jPanel2.add(btnSave);
        btnSave.setBounds(250, 80, 76, 28);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(20, 60, 570, 130);

        jLabel1.setText("Nombre Sucursal:");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(180, 20, 140, 30);

        lblSucursal.setFont(new java.awt.Font("Khmer OS", 0, 18)); // NOI18N
        jPanel1.add(lblSucursal);
        lblSucursal.setBounds(340, 20, 180, 30);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos"));
        jPanel3.setLayout(null);

        tblTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tblTabla);

        jPanel3.add(jScrollPane1);
        jScrollPane1.setBounds(40, 30, 500, 190);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(20, 200, 580, 230);

        btnSeleccionar.setText("Seleccionar");
        btnSeleccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeleccionarActionPerformed(evt);
            }
        });
        jPanel1.add(btnSeleccionar);
        btnSeleccionar.setBounds(30, 460, 140, 28);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 659, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 529, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(690, 559));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSeleccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeleccionarActionPerformed
        fila = tblTabla.getSelectedRow();
        try {
            cargarVenta();
        } catch (VacioException ex) {
            Logger.getLogger(FrmVentas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PosicionException ex) {
            Logger.getLogger(FrmVentas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSeleccionarActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        try {
            modificar();
        } catch (VacioException ex) {
            Logger.getLogger(FrmVentas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PosicionException ex) {
            Logger.getLogger(FrmVentas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FrmVentas.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TopeException ex) {
            Logger.getLogger(FrmVentas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void cargarTabla() {
        modelo.setLsVentas(listaV);
        tblTabla.setModel(modelo);
        tblTabla.updateUI();
    }

    private void limpiar() {
        //  this.control.setVenta(null);
        txtValor.setText("");
        lblMes.setText("");
        //fila = -1;
        cargarTabla();
    }

    //creamos metodo modificar para cargarlos e el boton
    public void modificar() throws VacioException, PosicionException, IOException, TopeException {
        if (!txtValor.getText().trim().isEmpty() && !lblMes.getText().isEmpty()) {
            try {
                HistorialControl histoyControl = new HistorialControl();
                Venta newVenta = new Venta();

                newVenta.setValor(Double.parseDouble(txtValor.getText()));
                newVenta.setId(venta.getId());
                newVenta.setMes(venta.getMes());

                sucursal.getLsVenta().modificar(newVenta, venta.getId());

                dao.modificar(sucursal, sucursal.getId());
                histoyControl.addHistory(newVenta);
                modelo.getLsVentas().imprimir();
                limpiar();
                JOptionPane.showMessageDialog(null, "Los datos se han actualizado");
            } catch (VacioException | PosicionException e) {
                JOptionPane.showMessageDialog(null, e.getMessage());
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un mes", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void cargarVenta() throws VacioException, PosicionException {
        fila = tblTabla.getSelectedRow();
        try {
            if (fila >= 0) {
                this.venta = sucursal.getLsVenta().get(fila);
                txtValor.setText(String.valueOf(venta.getValor()));
                lblMes.setText(venta.getMes().toString());
            }
        } catch (VacioException | PosicionException ex) {
            JOptionPane.showMessageDialog(null, "Tabla vacia", "Error", JOptionPane.ERROR_MESSAGE);
            System.out.println(ex.getMessage());
        }

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FrmVentas dialog = new FrmVentas(new javax.swing.JFrame(), true, null);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSeleccionar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblMes;
    private javax.swing.JLabel lblSucursal;
    private javax.swing.JTable tblTabla;
    private javax.swing.JTextField txtValor;
    // End of variables declaration//GEN-END:variables
}
