/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import controlador.util.Utilidades;
import javax.swing.table.AbstractTableModel;
import modelo.Sucursal;

/**
 *
 * @author ghuaba
 */
public class ModeloTablaSucursal extends AbstractTableModel {
    private Integer columnas = 4;
    private ListaEnlazada<Sucursal> lsSucursal = new ListaEnlazada<>();

    public ListaEnlazada<Sucursal> getLsSucursal() {
        return lsSucursal;
    }

    public void setLsSucursal(ListaEnlazada<Sucursal> lsSucursal) {
        this.lsSucursal = lsSucursal;
    }

    /**
     * @param datos the datos to set
     */
    public int getColumnCount() {
        return columnas;
    }

    public int getRowCount() {
        return lsSucursal.size();
    }

    @Override
    public Object getValueAt(int i, int i1) {
        if (lsSucursal != null && i >= 0 && i < lsSucursal.size()) {
            try {
                Sucursal s = lsSucursal.get(i);
                switch (i1) {
                    case 0:
                        return (s != null) ? s.getNombre() : "NO DEFINIDO";
                    case 1:
                        return (s != null) ? Utilidades.sumarVentas(s) : 0;
                    case 2:
                        return (s != null) ? Utilidades.mediaVentas(s) : 0;
                    case 3:
                        return (s != null) ? Utilidades.ventaMenor(s) : "NO DEFINIDO";
                    default:
                        return null;
                }
            } catch (VacioException | PosicionException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return null;

    }

    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "SUCURSAL";
            case 1:
                return "Venta anual";
            case 2:
                return "Venta promedio";
            case 3:
                return "Ventas Menores";
            default:
                return null;
        }

    }
}
