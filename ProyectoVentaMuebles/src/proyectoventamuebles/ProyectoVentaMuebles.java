/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package proyectoventamuebles;

import controlador.ed.lista.ListaEnlazada;
import controlador.ed.lista.exception.PosicionException;
import controlador.ed.lista.exception.VacioException;
import modelo.Sucursal;

/**
 *
 * @author ghuaba
 */
public class ProyectoVentaMuebles {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      ListaEnlazada<Sucursal> lista = new ListaEnlazada<>();

        try {

            Sucursal s1 = new Sucursal();

            s1.setId(lista.size() + 1);
            s1.setNombre("Afae ");

            lista.insertar(s1);
            lista.imprimir();

            s1 = new Sucursal();

            s1.setId(lista.size() + 1);
            s1.setNombre("Lizyta ");
            lista.insertar(s1);
            lista.imprimir();

            s1 = new Sucursal();

            s1.setId(lista.size() + 1);
            s1.setNombre("Tercero ");
            lista.insertar(s1);
            lista.imprimir();

            s1 = new Sucursal();

            s1.setId(lista.size() + 1);
            s1.setNombre("Cuarto ");
            lista.insertar(s1);
            lista.imprimir();
            s1 = new Sucursal();

            s1.setId(lista.size() + 1);
            s1.setNombre("Quinto a segundo ");
            lista.insertarPosicion(s1, 1);
            lista.imprimir();
            s1 = new Sucursal();

            s1.setId(lista.size() + 1);
            s1.setNombre("Sexto a inicio");
            lista.insertarInicio(s1);
            lista.imprimir();

        } catch (VacioException e) {
            System.out.println(e.getMessage());

        } catch (PosicionException e) {
            System.out.println(e.getMessage());
        }
    }
    
}
